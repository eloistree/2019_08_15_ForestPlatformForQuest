# How to use: 2019_08_15_ForestPlatformForQuest   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.forestplatformforquest":"https://gitlab.com/eloistree/2019_08_15_ForestPlatformForQuest.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.forestplatformforquest",                              
  "displayName": "Forest Platform For Quest",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Forest platform to start new small project.",                         
  "keywords": ["Script","Tool","Forest","Platform"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    